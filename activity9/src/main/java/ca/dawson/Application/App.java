package ca.dawson.Application;

import ca.dawson.Backend.*;

public class App 
{
    public static void main( String[] args )
    {
        RpsGame game1 = new RpsGame();

        game1.playRound("rock");
        System.out.println("Computer Wins: " + game1.getWins());
        System.out.println("Computer Losses: " + game1.getLosses());
        System.out.println("Computer Ties: " + game1.getTies());

        game1.playRound("paper");
        System.out.println("Computer Wins: " + game1.getWins());
        System.out.println("Computer Losses: " + game1.getLosses());
        System.out.println("Computer Ties: " + game1.getTies());

        game1.playRound("scissors");
        System.out.println("Computer Wins: " + game1.getWins());
        System.out.println("Computer Losses: " + game1.getLosses());
        System.out.println("Computer Ties: " + game1.getTies());
    }
}
