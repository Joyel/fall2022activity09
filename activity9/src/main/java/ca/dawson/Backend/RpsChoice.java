package ca.dawson.Backend;

import javafx.event.*;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField messages;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String playerChoice;
    private RpsGame game;

    public RpsChoice(TextField messages, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame game){
        this.messages = messages;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.playerChoice = playerChoice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e){
        String result = this.game.playRound(this.playerChoice);

        this.messages.setText(result);
        this.wins.setText("Wins: " + game.getWins());
        this.losses.setText("Losses: " + game.getLosses());
        this.ties.setText("Ties: " + game.getTies());
    }
}
