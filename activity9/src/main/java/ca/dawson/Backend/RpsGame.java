package ca.dawson.Backend;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rng;

    public RpsGame(){
        this.rng = new Random();
    }

    public int getWins(){
        return this.wins;
    }

    public int getTies(){
        return this.ties;
    }

    public int getLosses(){
        return this.losses;
    }

    public String playRound(String choice){
        int computerChoice = this.rng.nextInt(3);
        String outcome = "";

        if ((choice.equals("rock") && computerChoice == 0) || (choice.equals("paper") && computerChoice == 1) || (choice.equals("scissors") && computerChoice == 2)){
            this.ties++;
            outcome = "it's a tie.";
        }

        if ((choice.equals("rock") && computerChoice == 2) || (choice.equals("paper") && computerChoice == 0) || (choice.equals("scissors") && computerChoice == 1)){
            this.wins++;
            outcome = "the player won.";
        }

        if ((choice.equals("rock") && computerChoice == 1) || (choice.equals("paper") && computerChoice == 2) || (choice.equals("scissors") && computerChoice == 0)){
            this.losses++;
            outcome = "the computer won.";
        }

        String computerMove = "";

        if (computerChoice == 0){
            computerMove = "rock";
        }

        if (computerChoice == 1){
            computerMove = "paper";
        }

        if (computerChoice == 2){
            computerMove = "scissors";
        }

        return "Computer plays " + computerMove + " and " + outcome;
    }
}
