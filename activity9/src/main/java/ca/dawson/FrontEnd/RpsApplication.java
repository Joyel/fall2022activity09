package ca.dawson.FrontEnd;

import javafx.application.*;
import javafx.scene.paint.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import ca.dawson.Backend.*;

public class RpsApplication extends Application {
    private RpsGame game;

    @Override
   public void start(Stage stage) { 
        //container and layout
        Group root = new Group(); 

        //scene is associated with container, dimensions
        Scene scene = new Scene(root, 400, 300); 
        scene.setFill(Color.BLACK);

        //associate scene to stage and show
        stage.setTitle("Hello JavaFX"); 
        stage.setScene(scene); 
        stage.show(); 

        //initializing game
        this.game = new RpsGame();

        Button rockButton = new Button("rock");
        Button paperButton = new Button("paper");
        Button scissorsButton = new Button("scissors");
        HBox buttons = new HBox();
        buttons.getChildren().addAll(rockButton, paperButton, scissorsButton);
        
        TextField welcome = new TextField("Welcome!");
        welcome.setPrefWidth(300);
        TextField wins = new TextField("wins: 0");
        wins.setPrefWidth(200);
        TextField losses = new TextField("losses: 0");
        losses.setPrefWidth(200);
        TextField ties = new TextField("ties: 0");
        ties.setPrefWidth(200);
        HBox texts = new HBox();
        texts.getChildren().addAll(welcome, wins, losses, ties);
        
        VBox overall = new VBox();
        overall.getChildren().addAll(buttons, texts);
        
        root.getChildren().add(overall);
        
        RpsChoice rockChoice = new RpsChoice(welcome, wins, losses, ties, "rock", this.game);
        rockButton.setOnAction(rockChoice);
        
        RpsChoice paperChoice = new RpsChoice(welcome, wins, losses, ties, "paper", this.game);
        paperButton.setOnAction(paperChoice);
        
        RpsChoice scissorsChoice = new RpsChoice(welcome, wins, losses, ties, "scissors", this.game);
        scissorsButton.setOnAction(scissorsChoice);
    } 
        public static void main(String[] args) {
                Application.launch(args);
        }

    /*
     
     */
}
